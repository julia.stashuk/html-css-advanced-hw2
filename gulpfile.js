const gulp = require('gulp');
const { series } = require('gulp');
const sass = require('gulp-sass')(require('sass'));
const concat = require('gulp-concat');
const uglify = require('gulp-uglify');
var clean = require('gulp-clean');
const imagemin = require('gulp-imagemin');
const autoprefixer = require('gulp-autoprefixer');
const minifyjs = require('gulp-js-minify');
const rename = require("gulp-rename");
const cssmin = require('gulp-csso');
const size = require('gulp-size');
const sourcemaps = require('gulp-sourcemaps');
const browserSync = require("browser-sync").create();
const cleanCSS = require("gulp-clean-css");
const del = require('del');


const path = {
    src: {
        root: "./src",
        scss: "./src/scss",
        js: "./src/js",
        images: "./src/images/**",
        html: "./*.html",
        fonts: "./src/fonts/**/*.*",
    },
    dist: {
        root: "./dist",
        css: "./dist",
        js: "./dist",
        images: "./dist/images",
        html: "./dist",
        fonts: "./dist/fonts",
    },
    watch: {
        html: 'src/**/*.html',
        scss: 'src/scss/**/*.scss',
        js: 'src/js/**/*.js',
        images: 'src/img/**/*.{jpg,jpeg,png,gif,webp}',
        fonts: 'src/fonts/**/*.*',
}
}

const cleanDist = () => {
    return gulp.src('./dist/*', {read: false})
        .pipe(clean());
}

gulp.task('cleanDist', function () {
    return del('dist');
  });

function style() {
    return gulp
        .src(`${path.src.scss}/**/*.scss`) 
        .pipe(sourcemaps.init())
        .pipe(sass().on('error', sass.logError))
        .pipe(concat('styles.min.css'))
        .pipe(cleanCSS({
            level: 2
        }))
        .pipe(rename({
            basename: "styles",
            suffix: ".min",
        }))
        .pipe(autoprefixer({
            cascade: false
        }))
        .pipe(cssmin())
        .pipe(sourcemaps.write())
        .pipe(size({
            showFiles: true
        }))
        .pipe(gulp.dest(path.dist.css))
        .pipe(browserSync.stream());
}

function script() {
    return gulp.src ("./src/js/**/*.js")
    .pipe(concat('scripts.min.js'))
    .pipe(uglify())
    .pipe(gulp.dest('./dist'))
    .pipe(browserSync.stream());
}

function images() {
    return gulp
        .src(path.src.images)
        .pipe(
            imagemin({
                progressive: true,
                svgoPlugins: [{ removeViewBox: false }],
                interlaced: true,
                optimizationLevel: 3,
            })
        )
        .pipe(gulp.dest(path.dist.images))
        .pipe(browserSync.stream());
}

function fonts() {
    return gulp
    .src(path.src.fonts)
    .pipe(gulp.dest(path.dist.fonts))
    .pipe(browserSync.stream());
}


function server() {
    browserSync.init({
        server: {
            baseDir: "./",
        },
        notify: false,
        port: 3000,
    });
}


function watcher() {
    gulp.watch(path.watch.scss, style);
    gulp.watch(path.watch.js, script);
    gulp.watch(path.watch.images, images);
    gulp.watch(path.watch.fonts, fonts);
 };


const build = series(cleanDist, style, script, images, fonts);
const dev = series(build, gulp.parallel(watcher, server));

exports.build = build;
exports.dev = dev;